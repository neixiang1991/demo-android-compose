### 步骤

1. 用Android Studio创建项目，选择 Empty Compose Activity

2. 账号密码

```
10086
111111
```

### 注意

- dp和sp适配屏幕

```
见 DpScale.kt
```

- 使用三方库

```
// ViewModel
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:2.5.1"
// 网络请求
implementation "com.squareup.retrofit2:retrofit:2.9.0"
implementation "org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.0"
implementation "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:0.8.0"
```
