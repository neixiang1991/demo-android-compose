package com.example.demo_android_compose.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.demo_android_compose.network.LoginApi
import com.example.demo_android_compose.network.LoginRequest
import com.example.demo_android_compose.network.LoginResult
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/**
 * @Author: milin
 * @Create: 2023/6/24 13:37
 * @Desc  :
 */
class LoginViewModel : ViewModel() {

    private val _uiState = MutableStateFlow(LoginUiState())
    val uiState = _uiState.asStateFlow()

    var loginResult: LoginResult? by mutableStateOf(null)
        private set

    fun updateUsername(username: String) {
        _uiState.update { it.copy(username = username) }
    }

    fun updatePassword(password: String) {
        _uiState.update { it.copy(password = password) }
    }

    fun login() {
        viewModelScope.launch {
            try {
                _uiState.value.run {
                    val params = LoginRequest(username, password)
                    loginResult = LoginApi.retrofitService.login(params)
                }
            } catch (e: Exception) {
                loginResult = LoginResult(false, e.localizedMessage ?: "未知错误")
            }
        }
    }

    fun clearLoginResult() {
        loginResult = null
    }
}
