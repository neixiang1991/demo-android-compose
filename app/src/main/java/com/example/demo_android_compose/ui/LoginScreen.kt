package com.example.demo_android_compose.ui

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.demo_android_compose.R

@Composable
fun LoginScreen(loginViewModel: LoginViewModel = viewModel()) {
    val loginUiState by loginViewModel.uiState.collectAsState()
    var eyeOpen by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        NavLeft(Modifier.align(Alignment.TopStart))
        NavRight(Modifier.align(Alignment.TopEnd))

        Column(modifier = Modifier.fillMaxSize()) {
            Logo(modifier = Modifier.align(Alignment.CenterHorizontally))

            TextInput(
                value = loginUiState.username,
                onValueChange = { loginViewModel.updateUsername(it) },
                placeholder = "请输入手机号/邮箱",
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number, imeAction = ImeAction.Next
                )
            )

            TextInput(
                value = loginUiState.password,
                onValueChange = { loginViewModel.updatePassword(it) },
                placeholder = "请输入登录密码",
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                keyboardActions = KeyboardActions(onDone = {
                    focusManager.clearFocus()
                    if (!loginUiState.buttonDisabled) {
                        loginViewModel.login()
                    }
                }),
                visualTransformation = if (eyeOpen) VisualTransformation.None else PasswordVisualTransformation()
            ) {
                Image(
                    painter = painterResource(id = if (eyeOpen) R.drawable.icon_eye_1 else R.drawable.icon_eye_0),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(start = 22.dp)
                        .width(30.dp)
                        .height(30.dp)
                        .clickable(MutableInteractionSource(), indication = null) {
                            eyeOpen = !eyeOpen
                        })
                Divider(
                    modifier = Modifier
                        .padding(horizontal = 22.dp)
                        .height(26.dp)
                        .width(2.dp),
                    color = Color(0xFFE5E5E5)
                )
                Tip(text = "忘记密码")
            }

            Button(!loginUiState.buttonDisabled) {
                focusManager.clearFocus()
                loginViewModel.login()
            }
            Tip(text = "手机验证码登录", modifier = Modifier.padding(top = 32.dp, start = 64.dp))

            Box(modifier = Modifier.weight(1F))
            Row(modifier = Modifier.padding(start = 64.dp, end = 64.dp, bottom = 188.dp)) {
                Third(text = "微信登录", icon = R.drawable.icon_wechat, modifier = Modifier.weight(1F))
                Third(text = "支付宝登录", icon = R.drawable.icon_alipay, modifier = Modifier.weight(1F))
            }
        }

        loginViewModel.loginResult?.run {
            AlertDialog(
                onDismissRequest = { },
                title = { Text(text = if (success) "登录成功" else message) },
                confirmButton = {
                    Text(text = "确定",
                        modifier = Modifier
                            .padding(32.dp)
                            .clickable(MutableInteractionSource(), indication = null) {
                                loginViewModel.clearLoginResult()
                            })
                },
            )
        }
    }
}

@Composable
fun NavLeft(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.icon_back),
        contentDescription = null,
        modifier = modifier
            .padding(top = 38.dp, start = 32.dp)
            .width(18.dp)
            .height(30.dp)
    )
}

@Composable
fun NavRight(modifier: Modifier) {
    Text(
        text = "预约开通",
        modifier = modifier.padding(top = 40.dp, end = 32.dp),
        color = Color(0xFF666666),
        fontSize = 26.sp,
        fontWeight = FontWeight.W600,
    )
}

@Composable
fun Logo(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.logo_sqb),
        contentDescription = null,
        modifier = modifier
            .padding(top = 219.dp, bottom = 90.dp)
            .width(300.dp)
            .height(95.dp)
    )
}

@Composable
fun TextInput(
    value: String,
    onValueChange: (String) -> Unit,
    placeholder: String,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    content: @Composable () -> Unit = @Composable {}
) {
    Row(
        modifier = Modifier
            .padding(top = 30.dp, start = 64.dp, end = 64.dp)
            .height(99.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        BasicTextField(value = value,
            onValueChange = onValueChange,
            modifier = Modifier.weight(1F),
            textStyle = TextStyle(color = Color.Black, fontSize = 36.sp),
            singleLine = true,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            visualTransformation = visualTransformation,
            decorationBox = { innerTextField ->
                if (value.isEmpty()) {
                    Text(
                        placeholder,
                        color = Color(0xFF999999),
                        fontSize = 28.sp,
                        modifier = Modifier.padding(top = 4.dp)
                    )
                }
                innerTextField()
            })
        if (value.isNotEmpty()) {
            Image(painter = painterResource(id = R.drawable.icon_close),
                contentDescription = null,
                Modifier
                    .width(30.dp)
                    .height(30.dp)
                    .clickable(MutableInteractionSource(), indication = null) { onValueChange("") })
        }
        content()
    }
    Divider(
        modifier = Modifier
            .height(1.dp)
            .padding(horizontal = 64.dp),
        color = Color(0xFFe5E5E5)
    )
}

@Composable
fun Tip(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        modifier = modifier,
        color = Color(0xFF666666),
        fontSize = 26.sp,
    )
}

@Composable
fun Button(enabled: Boolean, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .padding(top = 63.dp, start = 64.dp, end = 64.dp)
            .fillMaxWidth()
            .height(88.dp)
            .clip(RoundedCornerShape(44.dp))
            .background(if (enabled) Color(0xFFEE9E00) else Color(0xFFf5e2b8))
            .clickable(enabled) { onClick() },
    ) {
        Text(
            text = "登录",
            modifier = Modifier.align(Alignment.Center),
            color = Color.White,
            fontSize = 32.sp,
        )
    }
}

@Composable
fun Third(text: String, @DrawableRes icon: Int, modifier: Modifier) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Image(
            painter = painterResource(id = icon),
            contentDescription = null,
            modifier = Modifier
                .width(90.dp)
                .height(90.dp)
                .border(1.dp, Color(0xFFe5E5E5), RoundedCornerShape(45.dp))
        )
        Text(
            text = text,
            modifier = Modifier.padding(top = 12.dp),
            color = Color(0xFF999999),
            fontSize = 22.sp
        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
    LoginScreen()
}