package com.example.demo_android_compose.ui

import android.content.res.Resources
import androidx.compose.ui.unit.*

/**
 * @Author: milin
 * @Create: 2023/6/20 12:41
 * @Desc  :
 */

val scale: (Float) -> Float = {
    Resources.getSystem().displayMetrics.widthPixels * it / (Resources.getSystem().displayMetrics.density * 750)
}

val Int.dp: Dp get() = scale(this.toFloat()).dp

val Int.sp: TextUnit get() = scale(this.toFloat()).sp
