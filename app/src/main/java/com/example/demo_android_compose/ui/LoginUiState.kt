package com.example.demo_android_compose.ui

/**
 * @Author: milin
 * @Create: 2023/6/24 13:38
 * @Desc  :
 */
data class LoginUiState(
    var username: String = "",
    var password: String = "",
) {
    val buttonDisabled: Boolean
        get() {
            return username.isEmpty() || password.length < 6
        }
}
