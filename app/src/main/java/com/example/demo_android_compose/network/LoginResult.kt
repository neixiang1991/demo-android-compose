package com.example.demo_android_compose.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * @Author: milin
 * @Create: 2023/6/24 15:24
 * @Desc  :
 */
@Serializable
data class LoginResult(
    val success: Boolean,
    @SerialName(value = "desc")
    val message: String,
)