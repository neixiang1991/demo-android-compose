package com.example.demo_android_compose.network

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * @Author: milin
 * @Create: 2023/6/24 14:56
 * @Desc  :
 */

private const val BASE_URL = "https://www.fastmock.site"

private val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(Json.asConverterFactory(MediaType.get("application/json")))
    .build()

interface LoginApiService {
    @POST("mock/639e18e794db09edd8f347dde6100fa7/demo/login")
    suspend fun login(@Body request: LoginRequest): LoginResult
}

object LoginApi {
    val retrofitService: LoginApiService by lazy {
        retrofit.create(LoginApiService::class.java)
    }
}